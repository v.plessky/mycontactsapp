import React, {useEffect, useState, useRef} from 'react';
// 
import {StyleSheet, TextInput, View, Alert} from 'react-native';
import * as Animatable from 'react-native-animatable';

// navigation and keuboard
import { useNavigation } from '@react-navigation/native'
import { useKeyboard } from '@react-native-community/hooks'

import Button from '../components/Button';
import Header from '../components/Header';
// Login
import { useLogin } from '../context'

// import {useAppState} from '@react-native-community/hooks';


const secure = {
  login: '123',
  password: '123',
};

const Screen2 = () => {
  const navigation = useNavigation();
  const inputRef = useRef();
  const [isFirst, setIsFirst] = useState(true);
  const [isAnimatable, setIsAnimatable] = useState(false);
  const [valueLogin, setValueLogin] = useState('');
  const [valuePassword, setValuePassword] = useState('');
  const [isLogin, setIsLogin] = useState('');

// const currentAppState = useAppState();

  const keyboard = useKeyboard()

  const { setUserLogin } = useLogin()

 // alert user when entered data is not correct 
  const createAlert = () =>
  Alert.alert('Please enter correct data', 'Incorrect Login / Password', [
    { text: 'OK', onPress: () => console.log('Incorrect login/password entered') }
  ])


//  const screenStrings = ['Screen 1', 'Screen 2', 'Screen 3'];

/*
  const storeUserLogin = async value => {
    try {
      await AsyncStorage.setItem('login', String(value));
    } catch (e) {
      console.log(e);
    }
  };

  const getUserLogin = async () => {
    try {
      const value = await AsyncStorage.getItem('login');
      if (value !== null) {
        setIsLogin(value);
      }
    } catch (e) {
      console.log(e);
    }
  };

  getUserLogin();
*/

//  console.log('currentAppState', currentAppState);

  const handleSecure = () => {
    if (valueLogin === secure.login && valuePassword === secure.password) {
      console.log('Login and password correct');

      // navigate to Screen 3
      navigation.navigate('Screen 3');

      // reset login and password to blank values
      setValueLogin('');
      setValuePassword('');
      // set state to "User logged"
      setUserLogin()
    } else {
      // alert user that entered data is wrong 
      createAlert()

      setIsAnimatable(!isAnimatable);
    }
  };

  useEffect(() => {
    // if it is first, set to 'not first;
    setIsFirst(!isFirst);
  }, []);

  useEffect(() => {
    !isFirst && inputRef.current.animate('swing', 800)
  }, [isAnimatable])


  return (
    <>
      <Header title="Log In" />
      
      <View style={[styles.root, { paddingBottom: keyboard.keyboardShown ? 40 : 0 }]}>

        <Animatable.View style={styles.inputBox} ref={inputRef}>
          <TextInput
            style={styles.inputStyle}
            placeholder="Enter Login"
            value={valueLogin}
            onChangeText={setValueLogin}
            blurOnSubmit
            // remove next line if password is alphanumeric
            // keyboardType="numeric"
          />
          <TextInput
            style={styles.inputStyle}
            placeholder="Enter Password"
            value={valuePassword}
            onChangeText={setValuePassword}
            // remove next line if password is alphanumeric
            // keyboardType="numeric"
            // hide password; uncomment line if you want to see password when it is typed
            secureTextEntry={true}
          />
        </Animatable.View>
        <Button handleSecure={handleSecure} title="Submit" />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputBox: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    paddingHorizontal: 20,
  },

  inputStyle: {
    width: '100%',
    height: 55,
    margin: 12,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    paddingLeft: 30,
    borderRadius: 20,
    fontSize: 18,
  },
});

export default Screen2;
