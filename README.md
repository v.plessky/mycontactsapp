
# MyContactsApp

Simple application to access contacts and display imаges in gallery.
Application developed with React Native.


## Required libraries and modules


### React Native Hooks
https://github.com/react-native-community/hooks


React Native APIs turned into React Hooks allowing you to access asynchronous APIs directly in your functional components.

**Installation with npm**

`npm install @react-native-community/hooks`


## React Navigation 
https://reactnavigation.org/docs/getting-started

**Installation with npm**

`npm install @react-navigation/native`

*Additional dependencies*

`npm install react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view`

*Stack navigator library*

**Installation with npm**

`npm install @react-navigation/stack`

*Bottom Tabs Navigator*

https://reactnavigation.org/docs/bottom-tab-navigator/

**Installation with npm**

`npm install @react-navigation/bottom-tabs`


## React Native Async Storage
https://react-native-async-storage.github.io/async-storage/docs/install

**Installation with npm**

`npm install @react-native-async-storage/async-storage`


## React Native Picker

React Native Picker component

https://github.com/react-native-picker/picker

**Installation with npm**

`$ npm install @react-native-picker/picker --save`

## React Native Swipe Gestures

React Native component for handling swipe gestures in up, down, left and right direction.

https://github.com/glepur/react-native-swipe-gestures

**Installation with npm**

`npm install -S react-native-swipe-gestures`


